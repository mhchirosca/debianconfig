-- Theme handling library
local beautiful = require("beautiful")

-- Standard awesome library
local awful = require("awful")
local gears = require("gears")

-- Notification library
local naughty = require("naughty")

local configFolder = gears.filesystem.get_configuration_dir()
local themeFolder = configFolder .. "default/"

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.

if(gears.filesystem.is_dir(themeFolder))
then
    beautiful.init(themeFolder .. "theme.lua")
else
    beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
end

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    awful.layout.suit.floating,
}
-- }}}
