-- Standard awesome library
local awful = require("awful")

-- Notification library
local naughty = require("naughty")

awful.spawn.with_shell("nitrogen --restore")
awful.spawn.with_shell("xinput set-prop 'DELL08EA:00 06CB:CCA8 Touchpad' 'libinput Tapping Enabled' 1")
awful.spawn.with_shell("xinput set-prop 'DELL08EA:00 06CB:CCA8 Touchpad' 'libinput Natural Scrolling Enabled' 1")

awful.spawn.with_shell("xrandr --setprovideroutputsource modesetting NVIDIA-0")
awful.spawn.with_shell("xrandr --auto")

-- naughty.notify({ preset = naughty.config.presets.critical,
--                 title = "Just Reloaded the WM",
--                 text = awesome.startup_errors })
