-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

-- EDIT: Import error handling
local error = require("error")

-- EDIT: Import Variable definitions
local variablesDef = require("variablesDef")

-- EDIT: Import Wibar
local widgetBar = require("widgetBar")

-- EDIT: Import Key & Mouse bindings
local bindings = require("bindings")

-- EDIT: Rules
local rules = require("rules")

-- EDIT: Signals
local signals = require("signals")

-- EDIT: Extra startup commands
local commands = require("commands")
