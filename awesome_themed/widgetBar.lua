-- Standard awesome library
local gears = require("gears")
local awful = require("awful")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

-- Widgets lain
local lain = require("lain")

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a sys-tray widget
mysystray = wibox.widget.systray()

-- {{{ CPU widget
local cpuWidget = {
  layout = wibox.layout.fixed.horizontal,
  lain.widget.cpu {
    settings = function()
      widget:set_markup("|  " .. cpu_now.usage .. "% |")
    end
  },
}

-- Battery widget.
local batteryWidget = lain.widget.bat {
  notify = "on",
  settings = function()

    iconBat = "N/A"

    if(bat_now.status == "Charging")
    then iconBat = "  " end

    if(bat_now.status == "Discharging")
    then
      if(bat_now.perc > 80) then iconBat = "  " end
      if(bat_now.perc <= 80 and bat_now.perc > 60) then iconBat = "  " end
      if(bat_now.perc <= 60 and bat_now.perc > 35) then iconBat = "  " end
      if(bat_now.perc <= 35 and bat_now.perc > 10) then iconBat = "  " end
      if(bat_now.perc <= 15) then iconBat = "  " end
      iconBat = iconBat .. bat_now.time
    end

    if(bat_now.status == "Full")
    then iconBat = "  " end

    widget:set_markup("| " .. bat_now.perc .. "%" .. iconBat .. " |")


  end -- function end
}

local memoryWidget = lain.widget.mem {
  settings = function()
      widget:set_markup("| Mem " .. mem_now.used .. " MiB |" )
  end
}


local volumeWidget = lain.widget.alsa {
  timeout = 1,
  settings = function()
      widget:set_markup("|  " .. volume_now.level .. " |" )
  end
}


local networkWidget = lain.widget.net {
  units = 1024,
  settings = function()
      widget:set_markup("|  ".. net_now.sent ..  "  " .. net_now.received ..  " |")
  end
}

-- {{{ Wibar


-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
--screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)

    -- Edit
    -- Wallpaper
    --set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "", "", "", "", "", "", "", "", "" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = tasklist_buttons,
        style    = {
          shape_border_width = 1,
          shape_border_color = '#777777',
          shape  = gears.shape.rounded_bar,
        }

    }

      -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            --mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        --nil,
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            --mykeyboardlayout,
            mysystray,
            cpuWidget,
            fsWidget,
            --tempWidget,
            memoryWidget,
            batteryWidget,
            volumeWidget,
            networkWidget,
            mytextclock,
            s.mylayoutbox,
        },
    }
end)
-- }}}
