# DebianConfig

## Summary:

* initial Mozilla Firefox config
* initial awesomeWM config (default, restructured, self-themed)
* debian post-installation script
* post-login system startup script
* non-free post-installation packages.
* vim config
* terminal config
* file-templates

## AwesomeWM Info && Config:

## Mozilla Firefox Config:

A copy of the default (privacy) configuration for Mozilla Firefox

## Setup Script:

A Debian post-instalation script that will install system-wide packages such as:

* xserver-xorg
* windows mangaer of choice (awesomewm)
* browser (Mozilla Firefox)
* file-manager (Thunar and/or PCManFM)
* terminal-emulator (Tilix and/or URxvt)
* other ...

## Non-free Setup Packages:

Propritary blobs/software/firmware:

* Discord
* GitKraken
* MS Teams
* Atom Github editor
* Nvidia-driver
* Telegram
* Zoom
* Caprine
* Skype
* Slack
* VMWare

## Vim Config:

## Daily Scripts:

## File Templates:

